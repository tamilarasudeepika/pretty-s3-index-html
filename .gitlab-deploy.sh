#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVERS
echo "$string"
array=(${string//,/})
echo "$array"
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do    
  echo "Deploy project on server ${array[i]}"    
  ssh ubuntu@${array[i]} "cd /var/www/ex2.underdev.in/public_html/pretty-s3-index-html && sudo git pull origin master"
  done
